# python

Functional, imperative, object-oriented, reflective programming language. https://python.org

[[_TOC_]]

# Books
* *Functional Python Programming*
  2022 (Third edition) Steven F. Lott, Ricardo Bánffy (Packt Publishing)
  * 2018 (Second edition) Steven F. Lott (Packt Publishing)
  * 2015-01 [First edition
    ](https://www.oreilly.com/library/view/functional-python-programming/9781784396992/)
* *Functional programming with Python : techniques for maintainable, modular, and testable Python code*
  2015 Chris Armstrong

# Unofficial documentation
* https://en.m.wikipedia.org/wiki/Python_(programming_language)

# Features
## Lazy evaluation
* [lazy_python](https://pypi.org/project/lazy_python/)

## Memory management
### Resource acquisition is initialization (RAII)
* [resource acquisition is initialization RAII
  ](https://google.com/search?q=python+resource+acquisition+is+initialization+RAII)

## Union Type
* [python union type](https://google.com/search?q=python+union+type)
* [*Do union types actually exist in python?*
  ](https://stackoverflow.com/questions/38854282/do-union-types-actually-exist-in-python)
  (2018)

# Paradigms
## Functional Programming
* [*Functional Programming HOWTO*](https://docs.python.org/3.9/howto/functional.html)
* [*Functors, Applicatives, And Monads In Pictures*
  ](https://github.com/dbrattli/OSlash/wiki/Functors,-Applicatives,-And-Monads-In-Pictures)
  (2019)
* [*Charming Python #13 (20000155): Functional Programming in Python*
  ](http://gnosis.cx/publish/programming/charming_python_13.html)
  2001 David Mertz

# Tools
## `pip`
* [*Why you really need to upgrade pip*
  ](https://pythonspeed.com/articles/upgrade-pip/)
  2021-02..2022-01 Itamar Turner-Trauring

# Applications for Python
## Math
* [*Math for Programmers*
  ](https://www.manning.com/books/math-for-programmers)
  3D graphics, machine learning, and simulations with Python
  2020 Paul Orland

## Web
* [*Web Frameworks for Python*
  ](https://wiki.python.org/moin/WebFrameworks)

### ASGI
* [*ASGI Documentation*](https://asgi.readthedocs.io/)
  (2018)
  * [*Introduction*](https://asgi.readthedocs.io/en/latest/introduction.html)
* [*An introduction to ASGI, Asynchronous Server Gateway Interface - Philip Jones - PyLondinium19*
  ](https://www.youtube.com/watch?v=t3gCK9QqXWU)
  2019-07  PyLondinium
* [*A collection of example ASGI applications*
  ](https://github.com/erm/asgi-examples)
  (2019)
* [*ASGI Usage in Channels*
  ](https://www.tutorialdocs.com/tutorial/django-channels/asgi.html)

## Web Application able to use uWSGI
* [Pyramid](https://docs.pylonsproject.org/projects/pyramid-cookbook/en/latest/deployment/asgi.html)


### WSGI
* [*A Comparison of Web Servers for Python Based Web Applications*
  ](https://www.digitalocean.com/community/tutorials/a-comparison-of-web-servers-for-python-based-web-applications)
  2013-10-28 O.S Tezer

### FastCGI
* [*Apache, FastCGI and Python*
  ](https://www.electricmonk.nl/docs/apache_fastcgi_python/apache_fastcgi_python.html)
  2017-10 Ferry Boender

### CGI
* [*CGI (Common Gateway Interface) Scripts*
  ](https://wiki.python.org/moin/CgiScripts)
  (2014)
* [*Five minutes to a Python CGI*
  ](http://gnosis.cx/publish/programming/feature_5min_python.html)
  2000-06 David Mertz

# Libraries
## Code-checking
### Type-checking
* [*Our journey to type checking 4 million lines of Python*
  ](https://blogs.dropbox.com/tech/2019/09/our-journey-to-type-checking-4-million-lines-of-python/)
  2019-05 Jukka Lehtosalo
* [*Python Type Checking (Guide)*
  ](https://realpython.com/python-type-checking/)
  (2020-01) Geir Arne Hjelle
* [pydantic](https://pypi.org/project/pydantic)
  * https://tracker.debian.org/pkg/pydantic
* facebook/[pyre-check](https://pypi.org/project/pyre-check)
  * [*Pyre: Fast Type Checking for Python*
    ](https://www.facebook.com/notes/protect-the-graph/pyre-fast-type-checking-for-python/2048520695388071/)
    2018-05 Protect the Graph

## Contracts
* [python-contracts](https://pypi.org/project/python-contracts/)

## Functional Programming
* [returns](https://pypi.org/project/returns)
  * [classes](https://pypi.org/project/classes)
  * [lambdas](https://pypi.org/project/lambdas)
* [dry-monads](https://pypi.org/project/dry-monads)
* [OSlash](https://pypi.org/project/OSlash)
---
* [*Do-notation for Python*
  ](http://blog.tpleyer.de/posts/2019-01-15-Do-notation-for-Python.html)
  2019-01 Tobias Pleyer 

## Parser combinator
* parser.combinators [wim_v12e/parser-combinators-py](https://gitlab.com/wim_v12e/parser-combinators-py)
  (2017)
* [*Pysec: Monadic Combinatoric Parsing in Python (aka Parsec in Python)*
  ](http://www.valuedlessons.com/2008/02/pysec-monadic-combinatoric-parsing-in.html)
  2008
* [monadic parser combinator library parsec python
  ](https://google.com/search?q=monadic+parser+combinator+library+parsec+python)
* [parsec](https://pypi.org/project/parsec) @pypi
* [*Parsec: A parser combinator library in Python*](https://pythonhosted.org/parsec/)
  2015-2016 He Tao

# Implementations
## Rust
* [RustPython](https://rustpython.github.io/)
  * Also WebAssembly

## WebAssembly
* [webassembly python](https://google.com/search?q=webassembly+python)

# Docker images
* [*Using Alpine can make Python Docker builds 50× slower*
  ](https://pythonspeed.com/articles/alpine-docker-python/)
  2020-01..2021-05 Itamar Turner-Trauring

# Examples
## Ansi colors
python -c 'print u"\u001b[31mHello\u001b[0mWorld"'
